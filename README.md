# Kubernetes Workshop
Lab 07: Running Jobs and CronJobs
---

## Instructions

### Ensure that your environment is clean

 - List existent deployments
```
kubectl get all
```

### Run Single Job

 - Let's create a job to calculate prime numbers between 0 and 110, but before take a look to the job definition
```
curl https://gitlab.com/sela-aks-workshop/lab-07/raw/master/single-job.yaml
```

 - Create the job 
```
kubectl apply -f https://gitlab.com/sela-aks-workshop/lab-07/raw/master/single-job.yaml
```

 - List existent jobs
```
kubectl get jobs
```

 - List existent pods
```
kubectl get pods
```

 - Inspect pod logs
```
kubectl logs <pod-id>
```

### Run Sequentially Jobs

 - Let's create the same job but multiple times (Sequentially), check the job definition
```
curl https://gitlab.com/sela-aks-workshop/lab-07/raw/master/sequentially-job.yaml
```

 - Open a new terminal to watch how de jobs are created 
```
kubectl get pods -l app=primes-sequentially -w
```

 - Create the job 
```
kubectl apply -f https://gitlab.com/sela-aks-workshop/lab-07/raw/master/sequentially-job.yaml
```

 - List existent jobs
```
kubectl get jobs/primes-sequentially
```

 - List existent pods
```
kubectl get pods
```

### Run Parallel Jobs

 - Let's create the same job but multiple times (in Parallel), check the job definition
```
curl https://gitlab.com/sela-aks-workshop/lab-07/raw/master/parallel-job.yaml
```

 - In a new terminal watch how de jobs are created 
```
kubectl get pods -l app=primes-parallel -w
```

 - Create the job 
```
kubectl apply -f https://gitlab.com/sela-aks-workshop/lab-07/raw/master/parallel-job.yaml
```

 - List existent jobs
```
kubectl get jobs/primes-parallel
```

 - List existent pods
```
kubectl get pods
```

### Run a Basic CronJob

 - Let's create a basic cron job that would write a Hello World every minute, check the job definition
```
curl https://gitlab.com/sela-aks-workshop/lab-07/raw/master/cron-job.yaml
```

 - Create the job 
```
kubectl apply -f https://gitlab.com/sela-aks-workshop/lab-07/raw/master/cron-job.yaml
```

 - Wait until the cronjob is triggered
```
kubectl get pods -w
```

 - Check the pod output
```
kubectl logs <pod-id>
```

### Cleanup

 - List existent resources
```
kubectl get all
```

 - Delete existent resources
```
kubectl delete all --all
```

 - List existent resources
```
kubectl get all
```
